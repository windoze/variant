#ifndef CXUTIL_VARIANT_HPP
#define CXUTIL_VARIANT_HPP

#pragma once

#include <cxutil/variant/recursive_wrapper.hpp>
#include <cxutil/variant/variant.hpp>
#include <cxutil/variant/visitor.hpp>
#include <cxutil/variant/compare.hpp>
#include <cxutil/variant/io.hpp>

#endif // CXUTIL_VARIANT_HPP
