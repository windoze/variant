#ifndef CXUTIL_TYPE_TRAITS_HPP
#define CXUTIL_TYPE_TRAITS_HPP

#pragma once

#include <cxutil/type_traits/traits.hpp>
#include <cxutil/type_traits/function_traits.hpp>
#include <cxutil/type_traits/tuple_traits.hpp>
#include <cxutil/type_traits/variant_traits.hpp>

#endif // CXUTIL_TYPE_TRAITS_HPP
